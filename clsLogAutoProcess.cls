VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsLogAutoProcess"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Public lLocal As Boolean, cDomain As String, cService As String, nProcessID As Long, cProcessResult As String, cProcessNotes As String
Private oHTTPReq As New ChilkatHttpRequest, http As New ChilkatHttp, xml As New ChilkatXml, nResult As Long
Private cXML As String, cLastError As String, xml2 As New ChilkatXml, cPort As String, nProcessRunID As Long
' usage
'    Dim clsLogAutoProcess As New clsLogAutoProcess
    
'    clsLogAutoProcess.Init
'    'clsLogAutoProcess.lLocal = True
'    clsLogAutoProcess.nProcessID = 3
'    clsLogAutoProcess.cProcessNotes = "testing start"
'    clsLogAutoProcess.LogAutoProcessStart
    
'    clsLogAutoProcess.cProcessResult = "Success"
'    clsLogAutoProcess.cProcessNotes = "final notes"
'    clsLogAutoProcess.LogAutoProcessFinish

Public Function Init()
    'nResult = http.UnlockComponent("STVSTR.CB10216_GBzbgtf0lH9e") 'UK2NETHttp_PnT7vvLuORAz")
    Dim cVersion As String
    cVersion = http.Version
    If cVersion = "9.5.0.78" Then
        nResult = http.UnlockComponent("OAKSFT.CB1022020_ddzCMuHRne55")
    Else
        nResult = http.UnlockComponent("STVSTR.CB10216_GBzbgtf0lH9e")
    End If
    
    If (nResult <> 1) Then
        MsgBox http.LastErrorText
        Init = False
    Else
        Init = True
    End If
    cDomain = "oakwebservices.co.uk"
    cService = "/oaksupport/index.php"
    cPort = "80"
    lLocal = False
    cProcessNotes = ""
    cProcessResult = ""
End Function
Public Function LogAutoProcessStart()
    Dim oResp As ChilkatHttpResponse
    cLastError = ""
    cXML = ""
    ' clear http request headers
    oHTTPReq.RemoveAllParams
    ' set request to use POST
    oHTTPReq.UsePost
    ' set domain, debug (localhost only) and database, table parameters
    If lLocal Then
        cDomain = "localhost"
        oHTTPReq.AddParam "DBGSESSID", "0@clienthost:7869"
        cPort = "8888"
    End If
    oHTTPReq.SetFromUrl cDomain + cService
    oHTTPReq.AddParam "account", "OAKS-UDGP"
    oHTTPReq.AddParam "action", "AutoProcessStart"
    oHTTPReq.AddParam "autoID", nProcessID
    oHTTPReq.AddParam "autoNotes", cProcessNotes
    ' make request
    Set oResp = http.SynchronousRequest(cDomain, cPort, "0", oHTTPReq)
    If (oResp Is Nothing) Then
        ' error in POST request
        LogAutoProcessStart = False
        cLastError = http.LastErrorText
    ElseIf oResp.StatusCode = 201 Or oResp.StatusCode = 200 Then
        ' get xml (or non xml) data returned
        cXML = oResp.BodyStr
        LoadXML
        nProcessRunID = xml.GetNthChildWithTag("recordCount", 0).Content
        LogAutoProcessStart = True
    Else
        ' error of some kind
        LogAutoProcessStart = False
        cLastError = "HTTP status " + Trim(Str(oResp.StatusCode))
    End If
End Function
Public Function LogAutoProcessFinish()
    Dim oResp As ChilkatHttpResponse
    cLastError = ""
    cXML = ""
    ' clear http request headers
    oHTTPReq.RemoveAllParams
    ' set request to use POST
    oHTTPReq.UsePost
    ' set domain, debug (localhost only) and database, table parameters
    If lLocal Then
        cDomain = "localhost"
        oHTTPReq.AddParam "DBGSESSID", "0@clienthost:7869"
        cPort = "8888"
    End If
    oHTTPReq.SetFromUrl cDomain + cService
    oHTTPReq.AddParam "account", "OAKS-UDGP"
    oHTTPReq.AddParam "action", "AutoProcessFinish"
    oHTTPReq.AddParam "autoID", nProcessRunID
    oHTTPReq.AddParam "autoResult", cProcessResult
    oHTTPReq.AddParam "autoNotes", cProcessNotes
    ' make request
    Set oResp = http.SynchronousRequest(cDomain, cPort, "0", oHTTPReq)
    If (oResp Is Nothing) Then
        ' error in POST request
        LogAutoProcessFinish = False
        cLastError = http.LastErrorText
    ElseIf oResp.StatusCode = 201 Or oResp.StatusCode = 200 Then
        LogAutoProcessFinish = True
    Else
        ' error of some kind
        LogAutoProcessFinish = False
        cLastError = "HTTP status " + Trim(Str(oResp.StatusCode))
    End If
End Function
Public Function LoadXML()
    ' load XML string into Chilkat control for later use
    nResult = xml.LoadXML(cXML)
    If (nResult <> 1) Then
        cLastError = xml.LastErrorText
        LoadXML = False
        MsgBox cLastError
    Else
        LoadXML = True
    End If

End Function


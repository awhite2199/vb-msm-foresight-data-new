VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSFTP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private sftp As New ChilkatSFtp, cSFTPHost As String, cSFTPUser As String, cSFTPPwd As String, cSFTPRemoteDir As String, cFTPError As String

Public Function Init(cHost As String, cUser As String, cPwd As String, cRemoteDir As String)
    Dim nResult As Long, lOK As Boolean
    lOK = True
    nResult = sftp.UnlockComponent("STVSTR.CB10216_GBzbgtf0lH9e") 'UK2NETSSH_RsU9MgnZpRnj")
    If (nResult <> 1) Then
        lOK = False
    End If
    If lOK Then
        cSFTPHost = cHost
        cSFTPUser = cUser
        cSFTPPwd = cPwd
        cSFTPRemoteDir = cRemoteDir
    End If
    cFTPError = ""
    Init = lOK
End Function

Public Function SFTPUpload(cSource As String, cDestination As String)
    Dim lOK As Boolean, nResult As Long, cHandle As String
    lOK = True
    '  Upload a file.
    '  Set some timeouts, in milliseconds:
    sftp.ConnectTimeoutMs = 60000
    sftp.IdleTimeoutMs = 180000
    
    '  Connect to the SSH server.
    '  The standard SSH port = 22
    '  The hostname may be a hostname or IP address.
    nResult = sftp.Connect(cSFTPHost, 22)
    If (nResult <> 1) Then
        lOK = False
        cFTPError = "Connect to host error" + Str(nResult) + "-" + sftp.LastErrorText
        Call Add2Log("    sftp:connect to " + cSFTPHost + " host error " + Str(nResult))
    End If

    If lOK Then
        '  Authenticate with the SSH server.  Chilkat SFTP supports
        '  both password-based authenication as well as public-key
        '  authentication.  This example uses password authenication.
        nResult = sftp.AuthenticatePw(cSFTPUser, cSFTPPwd)
        If (nResult <> 1) Then
            lOK = False
            cFTPError = "Authenticate error" + Str(nResult) + "-" + sftp.LastErrorText
            Call Add2Log("    sftp:authenticate error " + Str(nResult))
        End If
    End If
    If lOK Then
        '  After authenticating, the SFTP subsystem must be initialized:
        nResult = sftp.InitializeSftp()
        If (nResult <> 1) Then
            lOK = False
            cFTPError = "Initialise error" + Str(nResult)
            Call Add2Log("    sftp:initialise error " + Str(nResult))
        End If
    End If

    If lOK Then
        '  Open a file for writing on the SSH server.
        '  If the file already exists, it is overwritten.
        '  (Specify "createNew" instead of "createTruncate" to
        '  prevent overwriting existing files.)
        Call Add2Log("upload file: " + cSource + " to " + cSFTPRemoteDir + cDestination, True)
        cHandle = sftp.OpenFile(cSFTPRemoteDir + cDestination, "writeOnly", "createTruncate")
        If (cHandle = vbNullString) Then
            lOK = False
            cFTPError = "Open file error:" + cSource + " to " + cSFTPRemoteDir + cDestination
            Call Add2Log("    sftp:open file error:" + cSource + " to " + cSFTPRemoteDir + cDestination)
        End If
    End If
    
    If lOK Then
        '  Upload from the local file to the SSH server.
        nResult = sftp.UploadFile(cHandle, cSource)
        If (nResult <> 1) Then
            lOK = False
            cFTPError = "Upload error" + Str(nResult)
            Call Add2Log("    sftp:upload error " + Str(nResult))
        End If
    End If
    
    If (cHandle <> vbNullString) Then
        '  Close the file.
        nResult = sftp.CloseHandle(cHandle)
    End If
    SFTPUpload = lOK

End Function
Private Sub Add2Log(cAction As String, Optional lAddTime As Boolean)
    Dim nLogFile As Integer
    nLogFile = FreeFile
    Open App.Path + "\ShopExport.log" For Append As #nLogFile
    Print #nLogFile, cAction + IIf(lAddTime, " - " + Format(Now, "DD/MM/YYYY HH:MM:SS"), "")
    Close #nLogFile
End Sub
Public Function GetLastError()
    GetLastError = cFTPError
End Function
